#!/usr/bin/env python
# encoding: utf-8
'''
比较cv2.add()和numpy的+操作
比较cv2.substract和numpy的-操作
利用cv2.add和cv2.substract实现锐化操作
参考Operations on Arrays和Image Filtering
'''
import cv2
import numpy as np
from matplotlib import pyplot as plt
#uint8的范围为[0,255]
x=np.uint8([250])
y=np.uint8([20])
z=np.uint8([255])
#250+10 = 260 => 255
print cv2.add(x,y)
#250+10 = 260 % 256 = 4
print x+y
#250-255=-5=>0
print cv2.subtract(x,z)
#250-255=-5%256=251
print x-z

img=cv2.imread("../../images/robben.jpg")
#对原始图像进行模糊，参数越大模糊越大
blurred=cv2.blur(img,(9,9))
#锐化
shaped=cv2.add(img,cv2.subtract(img,blurred))
#opencv是BGR图像故此用[:,:,::-1]转换为RGB
plt.subplot(221),plt.imshow(img[:,:,::-1]),plt.title(u'原始图像')
plt.subplot(222),plt.imshow(blurred[:,:,::-1]),plt.title(u'模糊图像')
plt.subplot(223),plt.imshow(shaped[:,:,::-1]),plt.title(u'锐化图像')
plt.show()