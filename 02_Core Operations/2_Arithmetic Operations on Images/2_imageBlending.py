#!/usr/bin/env python
# encoding: utf-8
'''
用cv2.addWeighted混合图像
参考Operations on Arrays
'''
import cv2
from matplotlib import pyplot as plt
img1 = cv2.imread('../../images/ml.jpg')
img2 = cv2.imread('../../images/opencv-logo.jpg')
print img1.shape
print img2.shape
dst = cv2.addWeighted(img1,0.7,img2,0.3,0)
plt.subplot(221),plt.imshow(img1[:,:,::-1]),plt.title(u'图像1')
plt.xticks([]), plt.yticks([])
plt.subplot(222),plt.imshow(img2[:,:,::-1]),plt.title(u'图像2')
plt.xticks([]), plt.yticks([])
plt.subplot(223),plt.imshow(dst[:,:,::-1]),plt.title(u'混合图像')
plt.xticks([]), plt.yticks([])
plt.show()
#cv2.imshow('dst',dst)
#cv2.waitKey(0)
#cv2.destroyAllWindows()
