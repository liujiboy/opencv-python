#!/usr/bin/env python
# encoding: utf-8
'''
操作图像元素
'''
import cv2
img = cv2.imread('../../images/robben.jpg')
#读取(100,100)处像素的BGR值
px = img[100,100]
print px
#读取(100,100)处像素的B值
blue = img[100,100,0]
print blue
#将(100,100)处像素的值修改为(255,255,255)
img[100,100] = [255,255,255]
print img[100,100]
#返回图像区域
print img[280:340, 330:390]
#如果只访问1个像素点推荐使用item和itemset方法，比索引访问（例如img[100,100]）速度更快
#等价于img[10,10,2]
print img.item(10,10,2)
#等价于img[10,10,2]=100
img.itemset((10,10,2),100)
print img.item(10,10,2)
#返回图像的维度——(宽,高,通道数)
print img.shape
#返回图像的大小
print img.size
#返回img中元素的数据类型，一般情况下BGR图像为uint8类型
#请注意opencv中的很多图像操作可能会产生超过uint8数据范围的值，这些值将被截断，从而导致错误
#因此在计算时要特别留意数据类型
print img.dtype