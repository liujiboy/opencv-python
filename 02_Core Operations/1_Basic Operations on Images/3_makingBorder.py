#!/usr/bin/env python
# encoding: utf-8
'''
某些图像算法在处理图像的边界处时需要为图像添加边框
本程序演示各种添加边框的方法
假设abcdefgh是图像，以下是按照不同参数添加边框后的效果：
BORDER_REPLICATE:     aaaaaa|abcdefgh|hhhhhhh
BORDER_REFLECT:       fedcba|abcdefgh|hgfedcb
BORDER_REFLECT_101:   gfedcb|abcdefgh|gfedcba
BORDER_WRAP:          cdefgh|abcdefgh|abcdefg
BORDER_CONSTANT:      iiiiii|abcdefgh|iiiiiii  i表示任意颜色常数
参考
'''
import cv2
from matplotlib import pyplot as plt

BLUE = [255,0,0]
#默认情况下imread会抛弃所有的alpha值，故此opencv-logo.png上的透明区域都被填充为[0,0,0]
#opencv-logo.png上的文字是黑色的，故此在显示的图片上无法看到文字
img1 = cv2.imread('../../images/opencv-logo.png')

replicate = cv2.copyMakeBorder(img1,10,10,10,10,cv2.BORDER_REPLICATE)
reflect = cv2.copyMakeBorder(img1,10,10,10,10,cv2.BORDER_REFLECT)
reflect101 = cv2.copyMakeBorder(img1,10,10,10,10,cv2.BORDER_REFLECT_101)
wrap = cv2.copyMakeBorder(img1,10,10,10,10,cv2.BORDER_WRAP)
constant= cv2.copyMakeBorder(img1,10,10,10,10,cv2.BORDER_CONSTANT,value=BLUE)
#opencv是BGR图像故此用[:,:,::-1]转换为RGB
plt.subplot(231),plt.imshow(img1[:,:,::-1]),plt.title('ORIGINAL')
plt.subplot(232),plt.imshow(replicate[:,:,::-1]),plt.title('REPLICATE')
plt.subplot(233),plt.imshow(reflect[:,:,::-1]),plt.title('REFLECT')
plt.subplot(234),plt.imshow(reflect101[:,:,::-1]),plt.title('REFLECT_101')
plt.subplot(235),plt.imshow(wrap[:,:,::-1]),plt.title('WRAP')
plt.subplot(236),plt.imshow(constant[:,:,::-1]),plt.title('CONSTANT')

plt.show()