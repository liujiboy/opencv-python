#!/usr/bin/env python
# encoding: utf-8
'''
输出cv2中的所有包含EVENT的变量或者函数
cv2中包含EVENT的都是窗体事件
'''
import cv2
#dir函数返回cv2中包含的变化和函数名称
events=[i for i in dir(cv2) if 'EVENT' in i]
print events