#!/usr/bin/env python
# encoding: utf-8
'''
演示trackbard的使用
函数参考User Interface.pdf
'''
import cv2
import numpy as np

#trackbar的回调函数
def printPos(x):
    print x
img = np.zeros((300,512,3), np.uint8)
cv2.namedWindow('image')

#创建4个trackbar
cv2.createTrackbar('R','image',0,255,printPos)
cv2.createTrackbar('G','image',0,255,printPos)
cv2.createTrackbar('B','image',0,255,printPos)
switch = '0 : OFF \n1 : ON'
cv2.createTrackbar(switch, 'image',0,1,printPos)
while(1):
    cv2.imshow('image',img)
    k = cv2.waitKey(1) & 0xFF
    if k == 27:
        break
    #获取trackbar的位置
    r = cv2.getTrackbarPos('R','image')
    g = cv2.getTrackbarPos('G','image')
    b = cv2.getTrackbarPos('B','image')
    s = cv2.getTrackbarPos(switch,'image')
    if s == 0:
        img[:] = 0
    else:
        img[:] = [b,g,r]
cv2.destroyAllWindows()