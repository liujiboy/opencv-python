#!/usr/bin/env python
# encoding: utf-8
'''
用matplotlib显示图像
'''
import cv2
from matplotlib import pyplot as plt
#opencv的图像格式是BGR
bgr = cv2.imread('../../images/robben.jpg')
#用numpy的索引方法将BGR转为RGB，这种方法比用opencv的split再merge方便
rgb=bgr[:,:,::-1] 
#画两个子图分别显示BGR和RGB图像
#与opencv相反matplotlib可以正确显示RGB
#但不能正确显示BGR
plt.subplot(121),plt.imshow(bgr),plt.title('BGR')
#取消坐标
plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(rgb),plt.title('RGB')
plt.xticks([]), plt.yticks([])
plt.show()
